# TermComClient
Terminal Server Communication Testing Client using Socket.IO in VB.net

# Config
- edit server URL in TermComClient.exe.config
```
  <applicationSettings>
    <TermCom.My.MySettings>
      <setting name="wsUrl" serializeAs="String">
        <value>http://localhost:8080/peripheral</value>
      </setting>
    </TermCom.My.MySettings>
  </applicationSettings>
```
