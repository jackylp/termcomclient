﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Module JsonUtil
    Public MySerializer As New JsonSerializer() With {
        .DefaultValueHandling = DefaultValueHandling.Ignore,
        .NullValueHandling = NullValueHandling.Ignore
        }

    Public Function JsonSerialize(Of T)(ByRef content As T) As String
        Return JsonConvert.SerializeObject(content)
    End Function

    Public Function JsonDerialize(Of T)(ByRef str As String) As T
        Return JsonConvert.DeserializeObject(Of T)(str)
    End Function
End Module

Public Class TermJsonHeader
    <JsonProperty("type")>
    Public Property Type As String = ""
    <JsonProperty("deviceid")>
    Public Property Device As String = ""
    <JsonProperty("channelid")>
    Public Property Channel As String = ""
    <JsonProperty("function")>
    Public Property Func As String
    <JsonProperty("originid")>
    Public Property OriginId As String
    <JsonProperty("status", Required:=False)>
    Public Property Status As Integer
    <JsonProperty("errorCode")>
    Public Property ErrorCode As String

    Public Shared Function Create(type As String, device As String,
                                  channel As String, func As String,
                                  Optional originId As String = Nothing) As TermJsonHeader
        Return New TermJsonHeader With {
            .Type = type,
            .Device = device,
            .Channel = channel,
            .Func = func,
            .OriginId = originId
        }
    End Function

    Public Shared Operator =(a As TermJsonHeader, b As TermJsonHeader) As Boolean
        Return a.Type = b.Type AndAlso a.Device = b.Device AndAlso
            a.Channel = b.Channel AndAlso a.Func = b.Func
    End Operator

    Public Shared Operator <>(a As TermJsonHeader, b As TermJsonHeader) As Boolean
        Return a.Type <> b.Type OrElse a.Device <> b.Device OrElse
            a.Channel <> b.Channel OrElse a.Func <> b.Func
    End Operator
End Class

Public Class TermJsonSource
    <JsonProperty("header")>
    Public Property Header As TermJsonHeader
    <JsonProperty("payload")>
    Public Property Payload As JObject

    Public Shared Function Create(type As String, device As String,
                                  channel As String, func As String,
                                  Optional originId As String = Nothing) As TermJsonSource
        Return New TermJsonSource With {
            .Header = TermJsonHeader.Create(type, device, channel, func, originId),
            .Payload = New JObject()
        }
    End Function

    Public Function IsSameHeader(ByRef other As TermJsonSource) As Boolean
        Return Me.Header = other.Header
    End Function

    Function ToJson() As String
        Return JsonSerialize(Me)
    End Function

    Function ToJObject() As JObject
        Return JObject.FromObject(Me, MySerializer)
    End Function

End Class

#If 0 Then
Public Class TermJson
    <JsonProperty("room")>
    Public Property Room As String
    <JsonProperty("source")>
    Public Property Source As TermJsonSource

    Public Shared Function Create(room As String, type As String,
                                  device As String, channel As String,
                                  func As String) As TermJson
        Return New TermJson With {
            .Room = room,
            .Source = TermJsonSource.Create(type, device, channel, func)
        }
    End Function

    Public Function IsSameHeader(ByRef other As TermJson) As Boolean
        Return Me.Room = other.Room AndAlso Me.Source.Header = other.Source.Header
    End Function

    Public Shared Function Create(ts As TermJson) As TermJson
        Return New TermJson With {
            .Room = ts.Room,
            .Source = TermJsonSource.Create(ts.Source.Header.Type,
                                            ts.Source.Header.Device,
                                            ts.Source.Header.Channel,
                                            ts.Source.Header.Func)
        }
    End Function

    Function ToJson() As String
        Return JsonSerialize(Me)
    End Function

    Function ToJObject() As JObject
        Return JObject.FromObject(Me)
    End Function

End Class
#End If
