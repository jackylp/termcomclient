﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports IO = Quobject.SocketIoClientDotNet.Client.IO
Imports Socket = Quobject.SocketIoClientDotNet.Client.Socket

Module Program
    Private Property typeId = "HAS"
    Private Property roomId = "RR_scanner"
    Private Property connectState = 0   ' 0 - disconnected, 1 - connecting, 2 - connected
    Private Property webSocket As Socket

    Public Sub OnConnect(socket As Socket)
        Console.WriteLine("Connected to Socket.IO server")
        ' bind connected IO events
        socket.On("peripheral/registration",
            Sub(msg As JObject)
                Try
                    Dim res = msg.ToObject(Of TermJsonSource)()
                    'Dim res = JsonConvert.DeserializeObject(Of TermJsonSource)(msg)
                    Console.WriteLine("Received Registration Call: {0}", msg)
                Catch ex As Exception
                    Console.WriteLine("Message Parse Exception: {0}, {1}",
                                      msg, ex.Message)
                End Try
            End Sub
        ).On("peripheral/RR_scanner",
             Async Sub(msg As JObject)
                 Console.WriteLine("Got Message: {0}", "scan_form")
                 Try
                     Dim res = msg.ToObject(Of TermJsonSource)()
                     'Dim res = JsonConvert.DeserializeObject(Of TermJsonSource)(msg)
                     Console.WriteLine("Received Scan Form Call: {0}", msg)
                     Await Task.Delay(2000)
                     If connectState = 2 Then
                         Dim reply = TermJsonSource.Create(typeId, "scanner",
                                                           roomId, "start",
                                                           res.Header.OriginId)
                         ' added more sample indexing fields in payload
                         reply.Payload = res.Payload.DeepClone()
                         reply.Payload.Add("passportType", "02")
                         reply.Payload.Add("appType", "02")
                         reply.Payload.Add("doA", "03-11-2018")
                         'Dim replyJObj = reply.ToJObject
                         Dim replyJObj = reply.ToJson()
                         Console.WriteLine("Send Scan Form to server: {0}", reply.ToJObject())
                         socket.Emit("peripheral/RR_scanner", replyJObj)
                     End If
                 Catch ex As Exception
                     Console.WriteLine("Message Parse Exception: {0}, {1}",
                                       msg, ex.Message)
                 End Try
             End Sub
        )
        ' send registration request to server
        Dim request = TermJsonSource.Create(typeId, "scanner", roomId, "registration")
        'Dim reqObj = request.ToJObject()
        Dim reqObj = request.ToJson()
        Console.WriteLine("Send registration to Socket.IO server: {0}", request.ToJObject())
        socket.Emit("peripheral/registration", reqObj)
    End Sub

    Sub ConnectServer()
        Console.WriteLine("Connecting to {0}", My.Settings.wsUrl)
        webSocket = IO.Socket(My.Settings.wsUrl, New IO.Options() With {
            .Reconnection = True,
            .ForceNew = True
        })
        connectState = 1
        webSocket.On(Socket.EVENT_CONNECT,
                  Sub()
                      connectState = 2
                      OnConnect(webSocket)
                  End Sub
        ).On(Socket.EVENT_CONNECT_TIMEOUT,
            Sub()
                Console.WriteLine("Socket.IO Connection Timeout ... ")
                connectState = 0
            End Sub
        ).On(Socket.EVENT_RECONNECTING,
            Sub()
                Console.WriteLine("Reconnecting ... Waiting for Socket.IO Server ... ")
                connectState = 1
            End Sub
        ).On(Socket.EVENT_DISCONNECT,
            Sub()
                Console.WriteLine("Disconnected from Socket.IO server ... ")
                connectState = 0
                ' off all connected events
                webSocket.Off("peripheral/registration")
                webSocket.Off("peripheral/RR_scanner")
            End Sub
        ).On(Socket.EVENT_ERROR,
            Sub(err As Exception)
                Console.WriteLine("Error on Socket.IO: {0}", err.Message)
                connectState = 0
            End Sub
        )

    End Sub

    Sub Main()
        Console.WriteLine("Press Enter to quit.")
        ConnectServer()
        Console.ReadLine()
    End Sub

End Module
